package android.theappbusiness.com.app.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Zain on 17/01/15.
 */
public class User implements Parcelable {


    private String mName;
    private String mTitle;
    private String mBio;
    private String mPhotoUrl;

    public User() {
    }

    public String getName() {
        return mName;
    }

    public void setName(String aName) {
        mName = aName;
    }


    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String aTitle) {
        mTitle = aTitle;
    }

    public String getBio() {
        return mBio;
    }

    public void setBio(String aBio) {
        mBio = aBio;
    }


    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String aPhotoUrl) {
        mPhotoUrl = aPhotoUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(mName);
        parcel.writeString(mTitle);
        parcel.writeString(mBio);
        parcel.writeString(mPhotoUrl);
    }


    private User(Parcel in) {
        mName = in.readString();
        mTitle = in.readString();
        mBio = in.readString();
        mPhotoUrl = in.readString();

    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

}
