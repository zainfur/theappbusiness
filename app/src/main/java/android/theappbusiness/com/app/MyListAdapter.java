package android.theappbusiness.com.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.theappbusiness.com.app.objects.User;
import android.theappbusiness.com.app.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Zain on 17/01/15.
 */
public class MyListAdapter extends ArrayAdapter<User> {


    private static final String TAG = MyListAdapter.class.getSimpleName();

    private ArrayList<User> mUserList;
    private Context mContext;


    public MyListAdapter(Context context, int resource, ArrayList<User> objects) {
        super(context, resource, objects);
        mUserList = objects;
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }
        User user = mUserList.get(position);
        if (user != null) {

            viewHolder.nameTextView.setText(user.getName());
            viewHolder.titleTextView.setText(user.getTitle());
            viewHolder.bioTextView.setText(user.getBio());
            viewHolder.imageUrl = user.getPhotoUrl();

            new ImageDownloadAsyncTask().execute(viewHolder);

        }
        return convertView;
    }

    public static class ViewHolder {

        TextView nameTextView;
        TextView titleTextView;
        TextView bioTextView;
        ImageView photoImageView;
        String imageUrl;
        Bitmap bitmap;

        public ViewHolder(View aView) {

            nameTextView = (TextView) aView.findViewById(R.id.name);
            titleTextView = (TextView) aView.findViewById(R.id.title);
            bioTextView = (TextView) aView.findViewById(R.id.bio);
            photoImageView = (ImageView) aView.findViewById(R.id.image);

        }

    }

    private class ImageDownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

        private ViewHolder mViewHolder;


        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {

            mViewHolder = params[0];
            try {
                URL imageURL = new URL(mViewHolder.imageUrl);
                mViewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
            } catch (IOException e) {
                Log.e(TAG, "Downloading Image Failed");
                mViewHolder.bitmap = null;
            }

            return mViewHolder;
        }

        @Override
        protected void onPostExecute(ViewHolder result) {

            if (result.bitmap == null) {
                result.photoImageView.setImageResource(R.drawable.ic_launcher);
            } else {
                mViewHolder.photoImageView.setVisibility(View.VISIBLE);
                result.photoImageView.setImageBitmap(result.bitmap);
            }
        }
    }
}