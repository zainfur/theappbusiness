package android.theappbusiness.com.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.theappbusiness.com.app.objects.User;
import android.theappbusiness.com.app.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;


public class MyActivity extends Activity implements View.OnClickListener {


    private static final String TAG = MyActivity.class.getSimpleName();
    private static final String DATA_KEY = "data";

    //Member vars
    private ProgressDialog mProgressDialog;
    private ArrayList<User> mUsersList = new ArrayList<User>();
    private MyListAdapter mAdapter;


    protected void onSaveInstanceState(Bundle outState) {

        outState.putParcelableArrayList(DATA_KEY, mUsersList);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my);
//        Button button = (Button) findViewById(R.id.button);
//        button.setOnClickListener(this);

        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList(DATA_KEY) != null) {
            mUsersList = savedInstanceState.getParcelableArrayList(DATA_KEY);
        } else {
            getData();
        }

        ListView listView = (ListView) findViewById(R.id.list);
        mAdapter = new MyListAdapter(this, R.layout.list_item, mUsersList);
        listView.setAdapter(mAdapter);

    }


    private void getData() {

        RequestQueue queue = ((MyApplication) getApplication()).getRequestQueue(getApplicationContext());

        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();

        mProgressDialog = ProgressDialog.show(this, getString(R.string.loading_label),
                getString(R.string.please_wait_label), true);

        String url = getString(R.string.data_url);
        StringRequest dataRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response

                        Document doc = Jsoup.parse(response);
                        //Log.d(TAG, "onResponse: " + response);
                        parseData(doc);

                        mProgressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("TAG", "Error" + error.toString());
                        mProgressDialog.dismiss();
                    }
                }
        ) {

        };
        queue.add(dataRequest);
    }


    private void parseData(Document aDoc) {

        mUsersList = new ArrayList<User>();

        Element users = aDoc.getElementById("users");
        Elements col2 = users.getElementsByClass("col2");
        for (Element user : col2) {
            User item = new User();
            item.setName(user.getElementsByTag("h3").text());
            item.setBio(user.getElementsByClass("user-description").text());
            item.setPhotoUrl(((Elements) user.getElementsByTag("img")).attr("src"));


            Elements titleElement = ((Elements) user.getElementsByTag("p"));
            if (titleElement != null && titleElement.size() > 0) {
                item.setTitle(titleElement.get(0).text());
            }

            mUsersList.add(item);

        }
        Log.i(TAG, "Total items parsed : " + mUsersList.size());

        mAdapter.clear();
        mAdapter.addAll(mUsersList);
        mAdapter.notifyDataSetChanged();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick");
        getData();

    }
}
