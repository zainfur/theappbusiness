package android.theappbusiness.com.app;

import android.app.Application;
import android.content.Context;
import android.theappbusiness.com.app.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MyApplication extends Application {

    private RequestQueue mQueue;
    private static final String TAG = MyApplication.class.getSimpleName();


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "AppCreated");
    }

    public RequestQueue getRequestQueue(Context aContext) {
        if (mQueue == null) {
            mQueue = Volley.newRequestQueue(aContext);
        }
        return mQueue;
    }

}

