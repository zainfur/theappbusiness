# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Who's who @ TAB
* Version 1.0


### How do I get set up? ###

* Summary of set up - Download and import project into Android Studio. 
* Configuration - Standard. Android Studio 0.8.6 Beta.
* Dependencies - Volley(Networking library), JSoup 1.8.1 (for parsing HTML)
* Database configuration - NA.
* How to run tests - NA
* Deployment instructions - Standard
* Complied APK available at - https://drive.google.com/file/d/0B0VvITfmeLP-OXF3ZGo1RmEzLW8/view?usp=sharing

### Who do I talk to? ###

* Repo owner or admin - Zain Mohammed - zainfur.mohammed@gmail.com
* Tested on a Nexus 4 and 7 looks presentable

### Known Issues ###

* Need to switch to Fragments.
* No caching for images implemented as yet - but needs to be done.
* No storage of data - ContentProvider not used - due to lack of time. The data is currently held in memory.